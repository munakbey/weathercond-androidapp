package com.example.weathercon;


import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.example.verigonder.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

	TextView txt_clouds,txt_main,txt_desc,txt_temp,txt_humidity,txt_pressure,txt_temp_min,txt_temp_max,txt_sunrise,txt_sunset,txt_wind_speed,txt_wind_deg;
	Spinner spinner;
	ImageView icon_temp;
	String icon;
	int temp;
	private ArrayAdapter<String> dataAdapterforCity;
	private String[] cities={"Adana","Ad�yaman","Afyonkarahisar","A�r�","Amasya","Ankara","Antalya","Artvin","Ayd�n","Bal�kesir","Bilecik","Bing�l","Bitlis","Bolu","Burdur","Bursa","�anakkale","�ank�r�","�orum","Denizli","Diyarbak�r","Edirne","Elaz��","Erzincan","Erzurum","Eski�ehir","Gaziantep","Giresun","G�m��hane","Hakk�ri","Hatay","Isparta","Mersin","�stanbul","�zmir","Kars","Kastamonu","Kayseri","K�rklareli","K�r�ehir","Kocaeli","Konya","K�tahya","Malatya","Manisa","Kahramanmara�","Mardin","Mu�la","Mu�","Nev�ehir","Ni�de","Ordu","Rize","Sakarya","Samsun","Siirt","Sinop","Sivas","Tekirda�","Tokat","Trabzon","Tunceli","�anl�urfa","U�ak","Van","Yozgat","Zonguldak","Bayburt","Karaman","K�r�kkale","Batman","��rnak","Bart�n","Ardahan","I�d�r","Yalova","Karab�k","Kilis","Osmaniye","D�zce"};
	String city;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        description();
   createSpinner();
   
  
    }

    
    public void description(){
    	txt_clouds=(TextView)findViewById(R.id.textView1);
    	txt_main=(TextView)findViewById(R.id.textView2);
    	txt_desc=(TextView)findViewById(R.id.textView3);
    	txt_temp=(TextView)findViewById(R.id.textView4);
    	icon_temp=(ImageView)findViewById(R.id.imageView1);
    	spinner=(Spinner)findViewById(R.id.spinner1);
    	txt_humidity=(TextView)findViewById(R.id.textView5);    	
    	txt_pressure=(TextView)findViewById(R.id.textView6);
    	txt_temp_min=(TextView)findViewById(R.id.textView7);
    	txt_temp_max=(TextView)findViewById(R.id.textView8);
    	txt_sunrise=(TextView)findViewById(R.id.textView9);
    	txt_sunset=(TextView)findViewById(R.id.textView10);
    	txt_wind_speed=(TextView)findViewById(R.id.textView11);
    	txt_wind_deg=(TextView)findViewById(R.id.textView12);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void createSpinner(){
    	dataAdapterforCity=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cities);
    	dataAdapterforCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	spinner.setAdapter(dataAdapterforCity);
    	spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				city=parent.getSelectedItem().toString();
				selectCity();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
    		
		});
    
    }
    
    public void selectCity(){
    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get("http://api.openweathermap.org/data/2.5/weather?appid=0c42f7f6b53b244c78a418f4f181282a&q="+city, new AsyncHttpResponseHandler(){
    	
    		@Override
    		@Deprecated
    		public void onSuccess(int statusCode, Header[] headers,String content) {  		
    			super.onSuccess(statusCode, headers, content);
    			Toast.makeText(MainActivity.this, city, Toast.LENGTH_SHORT).show();
    			try {
					JSONObject alVeri=new JSONObject(content);
					 Toast.makeText(MainActivity.this,city, Toast.LENGTH_SHORT).show();
					txt_clouds.setText(alVeri.getJSONObject("clouds").getString("all"));
					JSONArray array_weather=alVeri.getJSONArray("weather");
					txt_main.setText(array_weather.getJSONObject(0).getString("main"));
					txt_desc.setText(array_weather.getJSONObject(0).getString("description"));			
					txt_temp.setText(String.valueOf(alVeri.getJSONObject("main").getInt("temp")-273)+"�C");
					txt_humidity.setText(alVeri.getJSONObject("main").getString("humidity"));
					txt_pressure.setText(alVeri.getJSONObject("main").getString("pressure"));
					txt_temp_min.setText(String.valueOf(alVeri.getJSONObject("main").getInt("temp_min")-273));
					txt_temp_max.setText(String.valueOf(alVeri.getJSONObject("main").getInt("temp_max")-273));
					txt_sunrise.setText(alVeri.getJSONObject("sys").getString("sunrise"));
					txt_sunset.setText(alVeri.getJSONObject("sys").getString("sunset"));
					txt_wind_speed.setText(alVeri.getJSONObject("wind").getString("speed"));
					txt_wind_deg.setText(alVeri.getJSONObject("wind").getString("deg"));
					
					
					icon=array_weather.getJSONObject(0).getString("icon");
					Picasso.with(MainActivity.this).load("http://openweathermap.org/img/w/"+icon+".png").into(icon_temp);	
				} catch (JSONException e) {				
				}
    		}
    		
    		@Override
    		public void onFailure(int arg0, Header[] arg1, byte[] arg2,
    				Throwable arg3) {
    			super.onFailure(arg0, arg1, arg2, arg3);
    			Toast.makeText(MainActivity.this, "HATA!",Toast.LENGTH_SHORT).show();
    			Toast.makeText(MainActivity.this, city, Toast.LENGTH_SHORT).show();
    		}
    		
    	});
    }
    
}

